package oma.intern.i18n;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.locks.ReentrantLock;

/*
 *Date : 13/08/2020 13:08:2020
 *User : faf_ext
 */
public class I18nManager {
    private static final ReentrantLock locker = new ReentrantLock();

    private static final Map<String, String> lang_en = new HashMap<>();
    private static final Map<String, String> lang_fr = new HashMap<>();
    private static final Map<String, String> lang_mg = new HashMap<>();

    public static void load(LangEnum lang, Properties properties){
        locker.lock();
        try {
            switch (lang){
                case EN:
                    lang_en.clear();
                    properties.forEach((key, value) -> lang_en.put(key.toString(), value.toString()));
                    break;
                case MG:
                    lang_mg.clear();
                    properties.forEach((key, value) -> lang_mg.put(key.toString(), value.toString()));
                    break;
                default:
                    lang_fr.clear();
                    properties.forEach((key, value) -> lang_fr.put(key.toString(), value.toString()));
                    break;
            }
        }
        finally {
            locker.unlock();
        }
    }

    public static Map<String, String> getLang(LangEnum lang){
        switch (lang){
            case EN:
                return Collections.unmodifiableMap(lang_en);
            case MG:
                return Collections.unmodifiableMap(lang_mg);
            default:
                return Collections.unmodifiableMap(lang_fr);
        }
    }
}
