package oma.intern.i18n;

import java.util.Map;

/*
 *Date : 13/08/2020 13:08:2020
 *User : faf_ext
 */
public class I18nHelper {
    public static String translate(String key, LangEnum lang){
       Map<String, String> current = I18nManager.getLang(lang);
       return current.get(key.trim());
    }
    public static String translate(String key, LangEnum lang, Map<String, String> values){
        String text = translate(key.trim(),lang);
        for (Map.Entry<String, String> entry : values.entrySet()) {
            String k = entry.getKey();
            String v = entry.getValue();
            text = text.replace("{" + k + "}", v);
        }

        return text;
    }
}
