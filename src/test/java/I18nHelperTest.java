import oma.intern.i18n.I18nHelper;
import oma.intern.i18n.LangEnum;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/*
 *Date : 14/08/2020 14:08:2020
 *User : faf_ext
 */
public class I18nHelperTest {

    @Test
    public void testTranslate(){
        new I18nManagerTest().testLoad();

        Assert.assertEquals("Hi", I18nHelper.translate("greet", LangEnum.EN));
        Assert.assertEquals("Salut", I18nHelper.translate("greet", LangEnum.FR));
        Assert.assertEquals("Akory", I18nHelper.translate("greet", LangEnum.MG));

        Map<String, String> placeholder = new HashMap<>();
        placeholder.put("nom", "Koto");
        Assert.assertEquals("Avia aty Koto", I18nHelper.translate("call", LangEnum.MG, placeholder));
    }
}
