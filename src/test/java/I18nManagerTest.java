import oma.intern.i18n.I18nManager;
import oma.intern.i18n.LangEnum;
import org.junit.Assert;
import org.junit.Test;

import java.util.Properties;

/*
 *Date : 14/08/2020 14:08:2020
 *User : faf_ext
 */
public class I18nManagerTest {
    @Test
    public void testLoad(){
        Properties en = new Properties();
        en.put("greet", "Hi");
        en.put("call", "I'm calling {nom}");

        Properties fr = new Properties();
        fr.put("greet", "Salut");
        fr.put("call", "J'appelle {nom}");

        Properties mg = new Properties();
        mg.put("greet", "Akory");
        mg.put("call", "Avia aty {nom}");

        I18nManager.load(LangEnum.EN, en);
        I18nManager.load(LangEnum.FR, fr);
        I18nManager.load(LangEnum.MG, mg);

        Assert.assertEquals(2, I18nManager.getLang(LangEnum.EN).size());
        Assert.assertEquals(2, I18nManager.getLang(LangEnum.FR).size());
        Assert.assertEquals(2, I18nManager.getLang(LangEnum.MG).size());
    }
}
