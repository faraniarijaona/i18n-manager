#### About
This is a module for managing i18n of application

Input : Lang, Properties

Function : Translate key to specified Lang, default lang : fr

#### Usage
1.  Load Properties using ```I18nManager.load(LangEnum, Properties)``` 
2.  Translate Key Loaded by using ```I18nHelper.translate(String key, LangEnum)``` 
or ```I18nHelper.translate(String key, LangEnum, Map params)```

#### Contact Developer
ffaraniarijaona@gmail.com